from django.shortcuts import render

def home(request):
    return render(request, 'main/home.html')

def hobi(request):
    return render(request, 'main/hobi.html')

def skill(request):
    return render(request, 'main/skill.html')

def pendidikan(request):
    return render(request, 'main/pendidikan.html')