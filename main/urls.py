from django.urls import path

from . import views

app_name = 'main'

urlpatterns = [
    path('', views.home, name='home'),
    path('home', views.home, name='home'),
    path('hobi', views.hobi, name='hobi'),
    path('skill', views.skill, name='skill'),
    path('pendidikan', views.pendidikan, name='pendidikan'),
]
